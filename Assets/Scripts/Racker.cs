﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racker : MonoBehaviour 
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "Racker";
	public bool VERBOSE = false;
    public const float EPSILON = 0.001f;

//---------------------------------------------------------------------------FIELDS:
	
    #region InspectorFields
    public GameObject[] BallPrefabs;
    #endregion

    [Range( 2, 10 )]
    public int BaseLen = 4;

    private int lastPrefabGenerated = 0;
    private float radius;
		
//---------------------------------------------------------------------MONO METHODS:

	void Start() 
	{
        radius = BallPrefabs[0].transform.localScale.x * 0.5f + EPSILON;
    }
		
	void Update()
    {
        if( Input.GetKeyDown( KeyCode.T ) )
        {
            RackStraightTetrahedron( BaseLen );
        }
        if( Input.GetKeyDown( KeyCode.O ) )
        {
            RackStraightOctohedron( BaseLen );
        }
        if( Input.GetKeyDown( KeyCode.R ) )
        {
            RackStraightTriangle( BaseLen );
        }
    }

//--------------------------------------------------------------------------METHODS:

    public Transform RackStraightTetrahedron( int baseLength )
    {
        Vector3 pivot;
        var points = ShapeGenerator.Tetrahedron( baseLength, radius, out pivot );
        return rack( points, pivot );
    }

    public Transform RackStraightTriangle( int baseLength )
    {
        Vector3 pivot;
        var points = ShapeGenerator.Triangle( baseLength, radius, out pivot );
        return rack( points, pivot );
    }

    public Transform RackStraightOctohedron( int baseLength )
    {
        Vector3 pivot;
        var points = ShapeGenerator.Octahedron( baseLength, radius, out pivot );
        return rack( points, pivot );
    }

//--------------------------------------------------------------------------HELPERS:
	
    // DEBUG HELPER
    private Transform createSphere( Vector3 center )
    {
        GameObject ball = GameObject.CreatePrimitive( PrimitiveType.Sphere );
        ball.transform.position = center;
        return ball.transform;
    }

    private Transform instantiateBall( Vector3 center )
    {
        //return createSphere( center );

        var ball = GameObject.Instantiate( BallPrefabs[lastPrefabGenerated],
                                           center,
                                           Quaternion.identity );
        lastPrefabGenerated++;
        if( lastPrefabGenerated >= BallPrefabs.Length )
        {
            lastPrefabGenerated = 0;
        }
        return ball.transform;
    }

    private Transform rack( List<Vector3> points, Vector3 pivot )
    {
        GameObject parent = new GameObject();
        parent.transform.position = pivot;
        parent.name = "Balls";
        foreach( Vector3 point in points )
        {
            Transform trans = instantiateBall( point );
            trans.parent = parent.transform;
        }
        return parent.transform;
    }
}