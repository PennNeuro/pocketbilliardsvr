﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

public static class ShapeGenerator
{
    
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG        = "BallPlacer";    
    public const float DIAHEDRAL_ANGLE  = 70.5288f;
    
//--------------------------------------------------------------------INNER CLASSES:

	private class PointTetrahedron
    {
        //----------------------FIELDS:

        public List<PointTriangle> Planes;

        public List<Vector3> Points
        {
            get
            {
                List<Vector3> points = new List<Vector3>();
                foreach( PointTriangle plane in Planes )
                {
                    points.AddRange( plane.Points );
                }
                return points;
            }
        }
        
        public Vector3 CenterOfBase
        {
            get
            {
                return Planes[0].Center;
            }
        }

        //----------------CONSTRUCTORS:

        public PointTetrahedron()
		{
			Planes = new List<PointTriangle>();
		}
    }

	private class PointTriangle
    {
        //----------------------FIELDS:

        public List<List<Vector3>> Rows;

        public List<Vector3> Points
        {
            get
            {
                List<Vector3> points = new List<Vector3>();

                foreach( List<Vector3> row in Rows )
                {
                    foreach( Vector3 point in row )
                    {
                        points.Add( point );
                    }
                }
                return points;
            }
        }

        public Vector3 Center
        {
            get
            {
                Vector3 a = Rows[0][0];
                Vector3 b = Rows[0].LastElement();
                Vector3 c = Rows.LastElement()[0];                

                return MyMath.CenterOfEquilateral( a, b, c );
            }
        }

        //----------------CONSTRUCTORS:

        public PointTriangle()
		{
			Rows = new List<List<Vector3>>();
        }        
    }


//---------------------------------------------------------------------------FIELDS:

//---------------------------------------------------------------------MONO METHODS:

//--------------------------------------------------------------------------METHODS:

    public static List<Vector3> Tetrahedron( int baseLength, float radius )
    {
        return createTetra( baseLength, Vector3.zero, radius ).Points;
    }

    public static List<Vector3> Tetrahedron( int baseLength, 
                                             float radius, 
                                             out Vector3 pivot )
    {
        PointTetrahedron tetra = createTetra( baseLength, Vector3.zero, radius );
        pivot = tetra.CenterOfBase;
        return tetra.Points;
    }

    public static List<Vector3> Triangle( int baseLength, float radius )
    {
        return createTriangle( baseLength, Vector3.zero, radius ).Points;
    }

    public static List<Vector3> Triangle( int baseLength, 
                                          float radius, 
                                          out Vector3 pivot )
    {
        PointTriangle tetra = createTriangle( baseLength, Vector3.zero, radius );
        pivot = tetra.Center;
        return tetra.Points;
    }

    public static List<Vector3> Octahedron( int baseLength, float radius )
    {
        Vector3 center;
        return Octahedron( baseLength, radius, out center );
    }

    public static List<Vector3> Octahedron( int baseLength, 
                                            float radius, 
                                            out Vector3 pivot )
    {
        // Create a pyramid with given base, add points to the list we'll return
        PointTetrahedron bigTetra = createTetra( baseLength, Vector3.zero, radius );
        List<Vector3> points = bigTetra.Points;

        // Create a smaller pyramid right above it
        Vector3 smallTetraStart = firstPosOfNextPlane( bigTetra.Planes[0] );
        PointTetrahedron smallPyramid = createTetra( baseLength - 1, 
                                                     smallTetraStart,
                                                     radius);


        // Rotate smaller pyramid around base to make it stick out in other direction
        pivot = bigTetra.CenterOfBase;
        Quaternion rotation = Quaternion.Euler( 0, 0, 180 );
        List<Vector3> lilPyramidPoints = smallPyramid.Points;
        foreach( Vector3 point in lilPyramidPoints )
        {
            points.Add( point.RotatedAroundPivot( pivot, rotation ) );

        }

        return points;

    }

//--------------------------------------------------------------------------HELPERS:

    private static PointTriangle createTriangle( int largestRow, 
                                                 Vector3 firstBallPosition, 
                                                 float radius )
	{
		PointTriangle plane = new PointTriangle();

		for( int i = largestRow; i > 0; i-- )
		{
			List<Vector3> nextRow = createRow( i, firstBallPosition, radius );
			plane.Rows.Add( nextRow );
			firstBallPosition = firstPosOfNextRow( nextRow, radius );
		}
		return plane;
	}

	private static PointTetrahedron createTetra( int largestRow, 
                                                 Vector3 firstBallPosition, 
                                                 float radius )
	{
		PointTetrahedron pyramid = new PointTetrahedron();

		for( int i = largestRow; i > 0; i-- )
		{
			PointTriangle plane = createTriangle( i, firstBallPosition, radius );
			pyramid.Planes.Add( plane );

			// No planes above the last ball
			if( i == 1 )   break;

			firstBallPosition = firstPosOfNextPlane( plane );
		}
		return pyramid;
	}

	private static List<Vector3> createRow( int rowLength, 
                                            Vector3 firstBallPos, 
                                            float radius )
	{
		List<Vector3> row = new List<Vector3>();
		Vector3 nextPosition = firstBallPos;
		for( int i = 0; i < rowLength; i++ )
		{
			row.Add( nextPosition );
			nextPosition.x += radius * 2;
		}
		return row;
	}
    
	private static Vector3 firstPosOfNextPlane( PointTriangle plane )
    {
        //   c
        //  / \
        // a - b
        Vector3 aPos = plane.Rows[0][0];
		Vector3 bPos = plane.Rows[0][1];
		Vector3 cPos = plane.Rows[1][0];
		
		Vector3 midAB = Vector3.Lerp( aPos, bPos, 0.5f );
        
        Quaternion rot = Quaternion.AngleAxis( DIAHEDRAL_ANGLE, Vector3.left );

		return MyMath.RotatedAroundPivot( cPos, midAB, rot );
	}

	private static Vector3 firstPosOfNextRow( List<Vector3> row, float radius )
	{
		if( row.Count < 1 )   
		{
            LOG_TAG.TPrint( "Problem in firstPosOfNextRow" );
			return Vector3.zero;
		}

		float x = row[0].x + radius;
		float z = row[0].z + Mathf.Sqrt( 3 ) * radius;
		
		return new Vector3( x, row[0].y, z );
	}
    
}