﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Teams
{
    NoTeamYet, Lows, Mids, Highs
}
