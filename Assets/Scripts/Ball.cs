﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

// Ball will have two colliders, one trigger (just for cue stick) and one physical
// (for everything else)
public class Ball : MonoBehaviour 
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "Ball";
	public bool VERBOSE = false;

//---------------------------------------------------------------------------FIELDS:
	
    #region SetInInspector
    // Extra force applied to other balls on contact
    public float StrikeForce = 1f;  
    public int Number = 0;
    #endregion

	public Vector3 Spin { get; private set; }

    public float Speed
    {
        get
        {
            return velocityTracker.SmoothedVelocity.magnitude;
        }
    }

    public Vector3 Velocity
    {
        get
        {
            return velocityTracker.SmoothedVelocity;
        }
    }

    public bool IsCueBall
    {
        get
        {
            return Number == 0;
        }
    }

    public bool IsTouchingRubber
    {
        get
        {
            return isCollidingWithRubber;
        }
    }

    public bool IsTouchingPlastic
    {
        get
        {
            return isCollidingWithPlastic;
        }
    }
    
    private bool isCollidingWithRubber, isCollidingWithPlastic;

    private VelocityTracker velocityTracker;

//---------------------------------------------------------------------MONO METHODS:
    
    void Awake()
    {
        velocityTracker = GetComponent<VelocityTracker>();
        if( velocityTracker == null )
        {
            velocityTracker = gameObject.AddComponent<VelocityTracker>();
        }
    }

    //void FixedUpdate()
    //{
    //    // TODO add spin force
    //}

    void OnTriggerEnter( Collider collider )
    {
        if( collider.name == "Plastic" )
        {
            isCollidingWithPlastic = true;
        }
        else if( collider.name == "Rubber" )
        {
            isCollidingWithRubber = true;
        }
    }

    void OnTriggerExit( Collider collider )
    {
        if( collider.name == "Plastic"  ||  collider.name == "Rubber" )
        {
            isCollidingWithPlastic = false;
            isCollidingWithRubber = false;
        }
    }

    void OnCollisionEnter( Collision collision )
    {
        if( applyForceIfBall( collision ) )   return;
        
        if ( reflectIfWall( collision ) )   return; 
    }
        
//--------------------------------------------------------------------------METHODS:

    public void ApplySpin( Vector3 spin )
    {
        Spin = spin;
    }

    public void Stop()
    {
        velocityTracker.Clear( transform.position );
        var rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }

//--------------------------------------------------------------------------HELPERS:
	
    // If collision has a Ball on it, this applies force to it and returns true
    private bool applyForceIfBall( Collision collision )
    {
        Ball ball = collision.gameObject.GetComponent<Ball>();
        if( ball == null )   return false;

        Vector3 normal = collision.contacts[0].normal;
        float dot = Vector3.Dot( velocityTracker.SmoothedVelocity, -normal );
        Vector3 force = -normal * dot * StrikeForce;

        // Apply force to other ball
        Rigidbody otherRigidbody = collision.gameObject.GetComponent<Rigidbody>();
        otherRigidbody.AddForce( force, ForceMode.Impulse );

        // Apply opposite force to this ball
        GetComponent<Rigidbody>().AddForce( -force, ForceMode.Impulse );

        return true;
    }

    private bool reflectIfWall( Collision collision )
    {
        Wall wall = collision.gameObject.GetComponent<Wall>();
        if( wall == null )   return false;
        if( VERBOSE )
        {
            LOG_TAG.TPrint( "Applying force to wall" );
        }

        // TODO take into account spin
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        Vector3 normal = collision.contacts[0].normal;
        if( rigidbody != null   &&   normal != Vector3.zero )
        {
            rigidbody.velocity = Vector3.Reflect( velocityTracker.SmoothedVelocity, normal );
        }
        return true;
    }
}