﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyViveUtility;
using MyUtility;

public class BridgeController : ViveController
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "BlankViveController";

//---------------------------------------------------------------------------FIELDS:
	
    #region Inspector Fields
    public Transform Cue;
    public Transform RightController;
    public Transform BridgeTip;
    #endregion

    private bool usingBridge;
		
//---------------------------------------------------------------------MONO METHODS:

	void Update()
	{
        if( TriggerDown )
        {
            usingBridge = true;
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Trigger Down!" );
        }
        if( TriggerUp )
        {
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Trigger Up!" );
            if( usingBridge )
            {
                releaseCue();
                usingBridge = false;
            }
        }
        if( usingBridge )
        {
            makeCueGoThroughBridgeTip();
        }
	}

//--------------------------------------------------------------------------METHODS:

//--------------------------------------------------------------------------HELPERS:

    private void makeCueGoThroughBridgeTip()
    {
        Cue.LookAt( BridgeTip );
    }

    private void releaseCue()
    {
        Cue.localRotation = Quaternion.identity;
    }
}
