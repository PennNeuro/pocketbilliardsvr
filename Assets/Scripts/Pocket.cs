﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

public class Pocket : MonoBehaviour 
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "Pocket";
	public bool VERBOSE = false;

//---------------------------------------------------------------------------FIELDS:
	
    public List<Ball> BallsInPocket { get; private set; }

    public bool HasBallsInPocket
    {
        get
        {
            return BallsInPocket.Count > 0;
        }
    }

    private Vector3 pocketCenter;

//---------------------------------------------------------------------MONO METHODS:

    void Awake()
    {
        BallsInPocket = new List<Ball>();
    }

    void OnCollisionEnter( Collision collision )
    {
        sinkIfBall( collision );        
    }
    
    void Start()
    {
        pocketCenter = gameObject.GetBounds().center;
    }

//--------------------------------------------------------------------------METHODS:

    public Ball[] TakeBallsOutOfPocket()
    {
        Ball[] balls = BallsInPocket.ToArray();
        BallsInPocket.Clear();
        return balls;
    }

//--------------------------------------------------------------------------HELPERS:
	
    // Returns true if collision is a ball and was sunk
    private bool sinkIfBall( Collision collision )
    {        
        Ball ball = collision.gameObject.GetComponent<Ball>();
        if( ball == null )   return false;
        BallsInPocket.Add( ball );
        ball.transform.position = pocketCenter;
        ball.Stop();
        return true;
    }
}