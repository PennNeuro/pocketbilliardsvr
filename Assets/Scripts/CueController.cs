﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyViveUtility;


public class CueController : ViveController
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "CueController";

//---------------------------------------------------------------------------FIELDS:
	
    #region Inspector Fields
    public Cue TheCue;
    #endregion
		
//---------------------------------------------------------------------MONO METHODS:

	void Update()
    {
        if( TriggerDown )
        {
            TheCue.FlyTowardsBalls();
        }
    }


//--------------------------------------------------------------------------METHODS:

//--------------------------------------------------------------------------HELPERS:
	
}