﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

//TODO Win if break and sink 8?
public class EightBallManager : BilliardsManager 
{    
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "EightBallManager";

//---------------------------------------------------------------------------FIELDS:
	
    private int activePlayerIndex;

    private Player activePlayer
    {
        get
        {
            return players[activePlayerIndex];
        }
    }

    private Player otherPlayer
    {
        get
        {
            if( activePlayerIndex == 0 )   return players[1];
            return players[0];
        }
    }

    private bool cueBallWasHitThisTurn;
    private bool playerSunkOneOfThierBalls;
    private bool playerScratched;
    private Player[] players;

    private int lowsSunk, highsSunk;

    // TODO will be bounds in which a player can place ball
    private Vector3 cueResetPosition;

    private int nonEightBallsToSinkPerPlayer;
    private bool isBreak;
		
//---------------------------------------------------------------------MONO METHODS:
		    
	void Start()
    {
        StartGame();
    }    

//------------------------------------------------------- BILLIARDS MANAGER METHODS:

    public override void StartGame()
    {
        players = new Player[2];
        players[0] = new Player();
        players[1] = new Player();
        highsSunk = 0;
        lowsSunk = 0;
        state = GameStates.Turn; //TODO will be placingCue

        // Doc two for eight and cue, divide by two for half the ball
        nonEightBallsToSinkPerPlayer = ( allBalls.Length - 2 ) / 2;
        rackEmUp();

    }

    public override void OnBallsStartedMoving()
    {
        if( VERBOSE )   LOG_TAG.TPrint( "Balls are moving!" );
        if( DEBUG )
        {
            ScreenPrinter.Instance.HideAllText();
            ScreenPrinter.Instance.Print( 0, "Balls Moving!" );
            ScreenPrinter.Instance.Print( 3, "Active Player: " + activePlayer.Team );
        }
    }

    public override void OnBallsStoppedMoving( Ball[] ballsSunkThisTurn )
    {
        if( VERBOSE )   LOG_TAG.TPrint( "Balls are stopped!" );
        if( DEBUG )  ScreenPrinter.Instance.Print( 0, "Balls Stopped! Sunk " + ballsSunkThisTurn.Length );
        // Determine which of the active player's ball were sunk, whether or not 
        // they scratched, and whether or not they sunk the 8
        List<Ball> activePlayersBalls = new List<Ball>();
        List<Ball> otherPlayersBalls = new List<Ball>();
        bool scratched = false;
        bool sunkEight = false;
        foreach( Ball ball in ballsSunkThisTurn )
        {
            if( ball.Number == 8 )  sunkEight = true;
            else if( ball.IsCueBall )  scratched = true;                
            else if( ( ball.Number > 8  &&  activePlayer.Team == Teams.Highs ) ||
                     ( ball.Number < 8  &&  activePlayer.Team == Teams.Lows ) ||
                     activePlayer.Team == Teams.NoTeamYet )
            {
                activePlayersBalls.Add( ball );
            }
            else  otherPlayersBalls.Add( ball );

            ball.gameObject.SetActive( false );
        }
        if( ! CueStick.LastStruckBall.IsCueBall )   scratched = true;

        if( VERBOSE )
        {
            LOG_TAG.TPrint( "scratched: " + scratched );
            LOG_TAG.TPrint( "sunkEight: " + sunkEight );
            LOG_TAG.TPrint( "activePlayersBalls: " + activePlayersBalls.Count );
            LOG_TAG.TPrint( "otherPlayersBalls: " + otherPlayersBalls.Count );
        }
        ////////////////////////////////////////////////////////////////////////////
        determineTurnOutcomes( activePlayersBalls, 
                               otherPlayersBalls, 
                               scratched, 
                               sunkEight );
    }

    public override void OnInactiveUpdate()
    {
        return;
    }

    public override void OnPlacingCueBall()
    {
        //TODO let player place ball
        PlaceCueBallArea.GetComponent<Collider>().enabled = true;
        cueball.transform.position = PlaceCueBallArea.gameObject.GetBounds().center;
        PlaceCueBallArea.GetComponent<Collider>().enabled = false;
        CueStick.Enable( true );
        state = GameStates.Turn;
    }
    
//--------------------------------------------------------------------------HELPERS:

    private void determineTurnOutcomes( List<Ball> activePlayersBalls, 
                                        List<Ball> otherPlayersBalls, 
                                        bool scratched, 
                                        bool sunkEight )
    {
        tallyLowsAndHighs( activePlayersBalls );
        tallyLowsAndHighs( otherPlayersBalls );

        bool shootingAtEight = playerSunkAllBalls( activePlayer );

        int totalBallsSunk = activePlayersBalls.Count + otherPlayersBalls.Count;

        if( scratched )
        {
            if( sunkEight  ||  shootingAtEight )
            {
                if( DEBUG ) ScreenPrinter.Instance.Print( 1, "Scratched on 8!" );
                if( VERBOSE ) LOG_TAG.TPrint( "Scratched on the 8!" );
                endGame( otherPlayer );
            }
            else if( activePlayer.Team != Teams.NoTeamYet )
            {
                if( DEBUG ) ScreenPrinter.Instance.Print( 1, "Scratched! no team yet, though" );
                putBallsBackOnTable( activePlayersBalls.ToArray() );
                swapActivePlayer();
            }
            state = GameStates.PlacingCueBall;
            return;
        }

        if( sunkEight )
        {
            if( isBreak )
            {
                LOG_TAG.TPrint( "WON ON BREAK!" );
                endGame( activePlayer );
            }
            else if( shootingAtEight ) endGame( activePlayer );
            else endGame( otherPlayer );
            return;
        }
        
        if( activePlayer.Team == Teams.NoTeamYet && totalBallsSunk > 0 )
        {
            letPlayerPickTeam( activePlayersBalls.ToArray() );
            if( DEBUG ) ScreenPrinter.Instance.Print( 1, "Picked a team for you: " + activePlayer.Team );
            if( VERBOSE ) LOG_TAG.TPrint( "New team! " + activePlayer.Team );
        }        
        else if( activePlayersBalls.Count == 0 )
        {
            swapActivePlayer();
        }

        CueStick.Enable( true );

        if( DEBUG ) ScreenPrinter.Instance.Toast( 3, "Cue Stick enabled", 1 );
        if( VERBOSE ) LOG_TAG.TPrint( "Enabling Cue Stick." );

        state = GameStates.Turn;
    }

    private void endGame( Player winningPlayer )
    {
        if( VERBOSE )
        {
            LOG_TAG.TPrint( winningPlayer.Team + " Wins!" );
        }        
    }
    
    // called after the balls stop moving
    private void endTurn()
    {
        if( ! playerSunkOneOfThierBalls )
        {
            swapActivePlayer();
        }          
        else if( playerScratched )
        {
            //TODO
            print( "Should create a new cue ball!" );
            swapActivePlayer();
        }
        playerSunkOneOfThierBalls = false;
        cueBallWasHitThisTurn = false;
    }

    private void letPlayerPickTeam( Ball[] ballsSunk )
    {
        //TODO let player actually pick team
        if( ballsSunk[0].Number < 8 )
        {
            activePlayer.Team = Teams.Lows;
            otherPlayer.Team = Teams.Highs;
        }
        else
        {
            activePlayer.Team = Teams.Highs;
            otherPlayer.Team = Teams.Lows;
        }
    }

    private bool playerSunkAllBalls( Player player )
    {
        if( player.Team == Teams.Highs )
        {
            return highsSunk == nonEightBallsToSinkPerPlayer;
        }
        return lowsSunk == nonEightBallsToSinkPerPlayer;
    }

    private void putBallsBackOnTable( Ball[] balls )
    {
        if( VERBOSE )
        {
            LOG_TAG.TPrint( "Taking some balls out of their pockets" );
        }
        //TODO Implement
            // Untally!
    }
        
    private void swapActivePlayer()
    {
        activePlayerIndex++;
        if( activePlayerIndex >= NumPlayers )
        {
            activePlayerIndex = 0;
        }
        if( DEBUG ) ScreenPrinter.Instance.Print( 2, "Active Player: " + activePlayer.Team );
    }

    private void tallyLowsAndHighs( List<Ball> balls )
    {
        foreach( Ball ball in balls )
        {
            if( ball.Number == 8  ||  ball.IsCueBall )  continue;
            if( ball.Number > 8 ) highsSunk++;
            else lowsSunk++;
        }
    }
}