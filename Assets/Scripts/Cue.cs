﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;
using NearestPoint;

public class Cue : MonoBehaviour 
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "Cue";
	public bool VERBOSE = false;

    private const float FILTER_WEIGHT = 0.3f;

//---------------------------------------------------------------------------FIELDS:
	
    #region SetInInspector
    public SphereCollider RubberCollider;

    public float Thwakiness = 1.0f;
    public float Rumbliness = 50.0f;
    public float Spininess = 1.0f;
    public int ControllerNumber = 0;
    public bool NeverDisableColliders;
    #endregion
    
    public bool StruckBall { get; private set; }

    public Ball LastStruckBall { get; private set; }

    private VelocityFilter filter;

    // Ray from back of cue to front of cue (Plastic and Rubber are on ignore 
    // raycast layer)
    private Ray cueRay
    {
        get
        {
            return new Ray( transform.position, transform.forward );
        }
    }
    private Rigidbody myRigidbody;
    //private CapsuleCollider PlasticCollider;

    // helpers for shooting the cue
    private Vector3 initialLocalPosition;
    private Transform startingParent;
    private bool isFlyingTowardsBalls;

    public float FlySpeed;
		
//---------------------------------------------------------------------MONO METHODS:

    void FixedUpdate()
    {
        filter.Update( RubberCollider.transform.position, Time.fixedDeltaTime );
    }

    void OnCollisionEnter( Collision collision )
    {
        if( isFlyingTowardsBalls )   StopFlyingTowardsBalls();
        print( "collision!" );

        //ushort pulseLen = (ushort)( filter.SmoothedVelocity.magnitude * Rumbliness );

        //SteamVR_Controller.Input( ControllerNumber ).TriggerHapticPulse( pulseLen );
    }

    void OnTriggerEnter( Collider other )
    {
        if( ! StruckBall  &&  strikeObject( other ) )
        {
            Enable( false );
        }
        
    }

    void OnTriggerStay( Collider other )
    {
        // We do this check again 
        if( ! StruckBall   &&   strikeObject( other ) )
        {
            Enable( false );
        }
    }
    
    void Start() 
	{
        filter = new VelocityFilter( FILTER_WEIGHT );
        startingParent = transform.parent;
        myRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //if( Input.GetKeyDown( KeyCode.Space ) )
        //{
        //    Enable( true );
        //}
        //Debug.DrawLine( transform.position, transform.position + filter.SmoothedVelocity );
        if( isFlyingTowardsBalls )
        {
            Vector3 force = FlySpeed * transform.forward * Time.deltaTime;
            myRigidbody.AddForce( force );
        }
    }
		

//--------------------------------------------------------------------------METHODS:

    public void Enable( bool enable )
    {
        if( ! NeverDisableColliders )
        {
            gameObject.EnableCollidersInChildren( enable );
        }
        StruckBall = ! enable;
        if( VERBOSE )   LOG_TAG.TPrint( "Enabled: " + enable );
    }

    public void FlyTowardsBalls()
    {
        transform.parent = null;
        isFlyingTowardsBalls = true;
        myRigidbody.isKinematic = false;
    }

    public void StopFlyingTowardsBalls()
    {
        transform.parent = startingParent;
        transform.localPosition = initialLocalPosition;
        transform.localRotation = Quaternion.identity;
        myRigidbody.isKinematic = true;
        myRigidbody.angularVelocity = Vector3.zero;
        myRigidbody.velocity = Vector3.zero;
        isFlyingTowardsBalls = false;
    }

//--------------------------------------------------------------------------HELPERS:
	
    private void strikeBallWithPlastic( Ball ball )
    {
        Rigidbody collisionRigidbody = ball.gameObject.GetComponent<Rigidbody>();
        collisionRigidbody.AddForce( Thwakiness * filter.SmoothedVelocity,
                                     ForceMode.Impulse );
    }

    private void strikeBallWithRubber( Ball ball )
    {
        Vector3 tipPos = RubberCollider.transform.position;
        Vector3 contact = SuperCollider.ClosestPointOnSurface( RubberCollider,
                                                               tipPos );
        float spin = 0;

        // Determine spin
        RaycastHit hit;
        if( Physics.Raycast( cueRay, out hit )  &&
            hit.collider.gameObject.GetComponent<Ball>() == ball )
        {
            print( "Rayed the ball, just gotta add spin now..." );
        }
        /////////////////

        Rigidbody collisionRigidbody = ball.gameObject.GetComponent<Rigidbody>();
        collisionRigidbody.velocity = Vector3.zero;
        collisionRigidbody.angularVelocity = Vector3.zero;
        collisionRigidbody.AddForce( Thwakiness * filter.SmoothedVelocity,
                                     ForceMode.Impulse );


    }

    // Applies appropriate force if other is a Ball.  Returns true if the ball
    private bool strikeObject( Collider other )
    {
        Ball ball = other.GetComponent<Ball>();

        if( ball == null )   return false;

        if( isFlyingTowardsBalls )   StopFlyingTowardsBalls();

        LastStruckBall = ball;

        if( ball.IsTouchingPlastic )
        {
            strikeBallWithPlastic( ball );
            return true;
        }
        else if( ball.IsTouchingRubber )
        {
            strikeBallWithRubber( ball );
            return true;
        }

        // If the ball isn't touching rubber or plastic, that means that this 
        // function was called in OnTriggerEnter, which was called before the 
        // ball's OnTriggerEnter.  Don't worry, it will be called again next
        // frame in OnTriggerStay
        return false;
    }
}