﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

public class Wall : MonoBehaviour
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "Wall";
	public bool VERBOSE = false;

//---------------------------------------------------------------------------FIELDS:
	
    public float Friction = 1;
    public float Bounciness = 1;
		
//---------------------------------------------------------------------MONO METHODS:

    void OnCollisionEnter( Collision collision )
    {
        if( VERBOSE )   LOG_TAG.TPrint( "Collision" );
    }

//--------------------------------------------------------------------------METHODS:

//--------------------------------------------------------------------------HELPERS:
	
}