﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

public abstract class BilliardsManager : MonoBehaviour
{    
    protected enum GameStates {  Inactive, Turn, BallsMoving, PlacingCueBall }

//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "BilliardsManager";
	public bool VERBOSE = false;
    
    public float BASICALLY_STOPPED = 0.01f;
    public const float MIN_TIME_MOVING = 0.1f;

    public bool DEBUG = false;
        
//---------------------------------------------------------------------------FIELDS:

    #region InspectorFields
    public Cue CueStick;
    public Pocket[] Pockets;
    public Transform RackPosition;
    public GameObject RackPrefab;
    public Transform PlaceCueBallArea;
    #endregion

    public int NumPlayers = 2;
    
    protected Ball[] allBalls
    {
        get
        {
            if( _allBalls == null )
            {
                findAllBalls();
            }
            return _allBalls;
        }
    }
    
    protected Ball cueball
    {
        get
        {
            if( _cueball == null )
            {
                findAllBalls();
            }
            return _cueball;
        }
    }    

    protected Ball eightBall
    {
        get
        {
            if( _eightBall == null )
            {
                findAllBalls();
            }
            return _eightBall;
        }
    }
    
    protected GameStates state = GameStates.Inactive;
    protected bool areBallsMoving
    {
        get
        {
            foreach( Ball ball in allBalls )
            {
                if( ball.isActiveAndEnabled   &&  
                    ! ball.Speed.NearlyEquals( 0, BASICALLY_STOPPED ) )
                {
                    return true;
                }
            }
            return false;
        }
    }
    
    private Ball[] _allBalls;
    private Ball _cueball;
    private Ball _eightBall;

    private List<Ball> ballsSunkThisTurn;
        
//---------------------------------------------------------------------MONO METHODS:
		    
    void Update()
    {
        switch( state )
        {
            case GameStates.Inactive:
                OnInactiveUpdate();
                break;

            case GameStates.PlacingCueBall:
                OnPlacingCueBall();
                break;

            case GameStates.Turn:
                if( CueStick.StruckBall )
                {
                    OnBallsStartedMoving();
                    // We time this so we don't end the turn before the balls
                    // have been moving for MIN_TIME_MOVING
                    Timer.Start( "bstmg" );
                    state = GameStates.BallsMoving;
                }
                break;

            case GameStates.BallsMoving:
                var ballsInPockets = pullBallsFromPockets();
                if( ballsInPockets.Length > 0 )
                {
                    ballsSunkThisTurn.AddRange( ballsInPockets );
                }

                if( ! areBallsMoving  &&  
                    Timer.TimeSinceStart( "bstmg" ) > MIN_TIME_MOVING )
                {
                    foreach( Ball ball in allBalls )  ball.Stop();
                    if( ballsSunkThisTurn != null )
                    {
                        OnBallsStoppedMoving( ballsSunkThisTurn.ToArray() );
                        ballsSunkThisTurn.Clear();
                    }
                    else   OnBallsStoppedMoving( new Ball[0] );
                }
                break;
        }
    }    

//-----------------------------------------------------------------ABSTRACT METHODS:

    /// <summary>
    /// Starts game.  Will probably call rackEmUp and set the state to Turn
    /// </summary>
    public abstract void StartGame();

    public abstract void OnBallsStartedMoving();

    /// <summary>
    /// Called when all balls stop moving
    /// </summary>
    /// <param name="ballsSunkThisTurn">The balls sunk this turn, in the order in
    ///                                 which they were sunk</param>
    public abstract void OnBallsStoppedMoving( Ball[] ballsSunkThisTurn );

    public abstract void OnInactiveUpdate();

    public abstract void OnPlacingCueBall();

//----------------------------------------------------------------PROTECTED METHODS:

    protected void placeCueBall()
    {
        state = GameStates.Turn;
    }

    protected void rackEmUp()
    {
        _allBalls = null;
        _cueball = null;
        _eightBall = null;

        ballsSunkThisTurn = new List<Ball>();

        GameObject.Instantiate( RackPrefab, 
                                RackPosition.position, 
                                Quaternion.identity );
    }
    
//--------------------------------------------------------------------------HELPERS:

    private void findAllBalls()
    {
        _allBalls = FindObjectsOfType<Ball>();

        foreach( Ball ball in _allBalls )
        {
            if( ball.IsCueBall )        _cueball = ball;
            if( ball.Number == 8 )      _eightBall = ball;
        }
    }

    private Ball[] pullBallsFromPockets()
    {
        List<Ball> balls = new List<Ball>();
        foreach( Pocket pocket in Pockets )
        {
            Ball[] ballsInPocket = pocket.TakeBallsOutOfPocket();
            if( ballsInPocket != null )
            {
                balls.AddRange( ballsInPocket );
            }
        }
        return balls.ToArray();
    }
}