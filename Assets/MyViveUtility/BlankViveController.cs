﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyViveUtility;
using MyUtility;

public class BlankViveController : ViveController
{
//------------------------------------------------------------------------CONSTANTS:

	private const string LOG_TAG = "BlankViveController";

//---------------------------------------------------------------------------FIELDS:
	
		
//---------------------------------------------------------------------MONO METHODS:

	void Update()
	{
        if( GripDown )
        {
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Grip Down!" );
        }
        if( GripUp )
        {
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Grip Up!" );
        }
        if( TriggerDown )
        {
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Trigger Down!" );
        }
        if( TriggerUp )
        {
            if( VERBOSE )   LOG_TAG.TPrint( Hand + " Trigger Up!" );
        }
	}

//--------------------------------------------------------------------------METHODS:

//--------------------------------------------------------------------------HELPERS:
	
}