﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyUtility;

namespace MyViveUtility
{
    [RequireComponent( typeof( SteamVR_TrackedObject ) )]
    public class ViveController : MonoBehaviour
    {
//------------------------------------------------------------------------CONSTANTS:

        private const string LOG_TAG = "ViveController";
        public bool VERBOSE = false;

//---------------------------------------------------------------------------FIELDS:

        /// <summary>
        /// Left or Right Hand?
        /// </summary>
        public Sides Hand { get; private set; }

        #region Input
        public float AxisX
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )    return 0;
                return theDevice.GetAxis().x;
            }
        }

        public float AxisY
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )    return 0;
                return theDevice.GetAxis().y;
            }
        }
        
        public bool Grip
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPress( SteamVR_Controller.ButtonMask.Grip );
            }
        }

        public bool GripDown
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPressDown( SteamVR_Controller.ButtonMask.Grip );
            }
        }

        public bool GripUp
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPressUp( SteamVR_Controller.ButtonMask.Grip );
            }
        }

        public bool Trigger
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPress( SteamVR_Controller.ButtonMask.Trigger );
            }
        }

        public bool TriggerDown
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPressDown( SteamVR_Controller.ButtonMask.Trigger );
            }
        }
        
        public bool TriggerUp
        {
            get
            {
                var theDevice = device;
                if( theDevice == null )   return false;
                return theDevice.GetPressUp( SteamVR_Controller.ButtonMask.Trigger );
            }
        }
        #endregion

        protected SteamVR_Controller.Device device
        {
            get
            {
                if( trackedObject == null )   return null;
                return SteamVR_Controller.Input( (int)trackedObject.index );
            }
        }

        protected SteamVR_TrackedObject trackedObject; 
        

//---------------------------------------------------------------------MONO METHODS:

        void Start()
        {
            initViveController();
        }


//--------------------------------------------------------------------------METHODS:

//----------------------------------------------------------------PROTECTED METHODS:

        protected void initViveController()
        {
            trackedObject = GetComponent<SteamVR_TrackedObject>();
            Hand = determineHand();
        }

        protected void triggerHapticPulse( ushort durationMS )
        {
            var theDevice = device;
            if( theDevice != null )
            {
                theDevice.TriggerHapticPulse( durationMS );
            }
        }

//--------------------------------------------------------------------------HELPERS:

        // Expects controller name to be unchanged (contain left or right)
        private Sides determineHand()
        {
            if( name.Contains( "left" ) )
            {
                return Sides.Left;
            }
            else if( name.Contains( "right" ) )
            {
                return Sides.Right;
            }
            LOG_TAG.TPrint( "Unable to determine if " + name + " is left or right" );
            return Sides.Neither;

        }
    }
}